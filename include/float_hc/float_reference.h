#ifndef FLOAT_REFERENCE_H
#define FLOAT_REFERENCE_H

#include <ros/ros.h>
#include <geometry_msgs/Quaternion.h>
#include <tf/transform_broadcaster.h>

#include "float_hc/Hovercraft.h"
#include "float_hc/Reference.h"
#include "float_hc/Params.h"
#include "float_hc/float_defines.h"

namespace float_hc
{

class FloatReference : public Hovercraft
{
public:
  FloatReference(ros::NodeHandle n);
  ~FloatReference();

public:
  bool reference_started;

private:
  ros::NodeHandle nh;
  ros::Publisher reference_pub;
  ros::ServiceServer params_srv;
  ros::Time start_time, actual_time;

  float_hc::Reference reference_msg;
  tf::TransformBroadcaster tf_br;
  tf::StampedTransform reference_tf;

  enum ReferenceType {POINT = 1, LINEAR = 2, LISSAJOUS = 3} reference_type;

  struct LissajousTrajectory
  {
    double T;          // Period [s]
    double w;          // Frequency [rad/s]
    int n_x, n_y;     // Frequency multipliers [-]
    double R_x, R_y;   // Radii [m]
    double x_o, y_o;   // Center of the figure [m]
  } liss_traj;

  struct LinearTrajectory
  {
    double v;          // Velocity [m/s]
    double phi;        // Orientation [rad]
    double x_o, y_o;   // Initial position [m]
  } lin_traj;

public:
  void calculateReferenceTrajectory();
  void publishReferenceMessage();
  void publishReferenceTransform();

private:
  bool paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res);
};

} // namespace float_hc

#endif // FLOAT_REFERENCE_H
