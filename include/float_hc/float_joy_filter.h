#ifndef FLOAT_JOY_FILTER_H
#define FLOAT_JOY_FILTER_H

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include "float_hc/Hovercraft.h"

namespace float_hc
{

class JoyFilter : public Hovercraft
{
public:
  JoyFilter(ros::NodeHandle n);
  ~JoyFilter();

private:
  ros::NodeHandle nh;
  ros::Publisher joy_pub;
  ros::Subscriber joy_sub;
  ros::ServiceServer params_srv;

public:
  void publishMessage();

private:
  void JoyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);
  
};

} // namespace float_hc

#endif // FLOAT_JOY_FILTER_H
