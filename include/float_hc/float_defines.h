#ifndef FLOAT_DEFINES_H
#define FLOAT_DEFINES_H

/*
 * gasdfasdfasdfasd
 */

namespace float_hc
{

// ROS parameters
const double TP = 0.01;           // [s] Sampling time
const double TP_SCN = 0.1;        // [s] Scanner update interval
const int SYS_FREQ = 100;         // [Hz] System rate

// Float parameters
const double FORCE_ARM = 0.115;                           // [m]
const double MIN_ENGINE_THRUST = 0.215;                   // [N] (0.152 if only one engine works)
const double MAX_ENGINE_THRUST = 4.0;                     // [N] (MAX is 6.32)
const double MIN_THRUST = 2*MIN_ENGINE_THRUST;            // [N]
const double MAX_THRUST = 2*MAX_ENGINE_THRUST;            // [N]
const double MIN_TORQUE = 2*MIN_ENGINE_THRUST*FORCE_ARM;  // [Nm]
const double MAX_TORQUE = 2*MAX_ENGINE_THRUST*FORCE_ARM;  // [Nm]
const double MAX_THROTTLE = 75.0;                         // [%]

const double MASS = 4.55 + 0.5;       // [kg]     Mass of the vehicle
const double INERTIA = 0.476 + 0.024; // [kg m^2] Moment of inertia about center of mass
const double BASE_LENGTH = 0.28;      // [m]      Length between center of mass and scanner


// State estimator parameters
// Measurement variances (data collected with u_3 = 40 % over 15 s samples)
const double PSI_SCN_VAR = 1.2534e-006;   // Variance of psi measurement noise (from scanner)
const double POS_SCN_VAR = 1.1282e-006;   // Variance of position measurement noise
const double VEL_SCN_VAR = 1.9972e-005;   // Variance of velocity measurement noise
const double GYRO_VAR = 4.5975e-007;      // Variance of angular rate measurement noise

// Process variances
const double PSI_VAR = 5.00e-010;    // Variance of psi
const double PSI_P_VAR = 2.00e-008;  // Variance of psi_p
const double Z_R_VAR = 1.00e-006;    // Variance of z_r
const double Z_R_P_VAR = 1.00e-006;  // Variance of z_r_p

const double POS_VAR = 1.00e-009;    // Variance of position
const double VEL_VAR = 1.00e-008;    // Variance of velocity
const double ACC_VAR = 1.00e-006;    // Variance of disturbance acceleration

// Measurement offsets
const double PSI_P_GYRO_OFFSET = 0.0081;


// Controller parameters
const int CONTROL_TYPE = 2; // 1 console, 2 manual, 3 automatic

const double ZETA = 1.0;    // [-] Damping coefficient
const double T_S_U = 3.0;   // [s] Settling time for surge
const double T_S_R = 4.0;   // [s] Settling time for rate
const double T_F = 0.5;     // [s] Filter constant for manual controls

const double K_P_U = 5*MASS/T_S_U;
const double K_D_U = MASS;
const double K_P_R = 2*INERTIA*4/T_S_R;
const double K_I_R = 16*INERTIA/(ZETA*ZETA*T_S_R*T_S_R);
const double K_D_R = INERTIA;

const double ROLL2RATE = 0.3;
const double PITCH2SPEED = 0.3;

const double K_P = 1.0;
const double K_B = 2.0;
const double AC_THROTTLE = 40.0;

// Reference generator parameters
const int REFERENCE_TYPE = 1; // 1 point, 2 linear, 3 lissajous

const double LIN_PHI = 0;    // [rad] Linear orientation
const double LIN_V = 0.1;    // [m/s] Linear speed

const double LIS_R_X = 0.5;  // [m] Lissajous X radius
const double LIS_R_Y = 0.5;  // [m] Lissajous Y radius
const double LIS_T = 60.0;   // [s] Lissajous period
const double LIS_N_X = 1;    // [-] Lissajous X multiplier
const double LIS_N_Y = 1;    // [-] Lissajous Y multiplier

} // namespace float_hc

#endif
