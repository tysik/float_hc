#ifndef FLOAT_STM_H
#define FLOAT_STM_H

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Quaternion.h>
#include <tf/transform_listener.h>

#include "float_hc/Params.h"
#include "float_hc/Controls.h"
#include "float_hc/RS232.h"
#include "float_hc/float_defines.h"

namespace float_hc
{

typedef unsigned int UINT;
typedef unsigned char UCHAR;
typedef unsigned short int USHORT;

class FloatSTM
{
public:
  FloatSTM(ros::NodeHandle n);
  ~FloatSTM();

private:
  ros::NodeHandle nh;

  ros::Publisher imu_pub;
  ros::Subscriber controls_sub, joy_sub;
  ros::ServiceServer params_srv;

  sensor_msgs::Imu imu_msg;

  // Data obtained from STM via UART
  struct STMData
  {
    double roll, pitch, yaw;
    double angular_rate[3];
    double linear_acc[3];
    UCHAR mode;
  } stm_data;

  enum { TX_BUFFER_SIZE = 11, RX_BUFFER_SIZE = 31 };

  UINT UART_port;
  UCHAR TX_buffer[TX_BUFFER_SIZE];
  UCHAR RX_buffer[RX_BUFFER_SIZE];

public:
  void performTransmission();
  void publishImu();

private:
  void controlsCallback(const float_hc::Controls::ConstPtr& controls_msg);
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);
  bool paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res);

  void prepareTXBuffer(double u_1, double u_2, double u_3);
  void openUART();
  void closeUART();
  void readData();
  void sendData();
  void convertRawData();
};

} // namespace float_hc

#endif // FLOAT_STM_H
