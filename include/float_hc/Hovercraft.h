#ifndef HOVERCRAFT_H
#define HOVERCRAFT_H

#include "float_defines.h"

namespace float_hc
{

class Hovercraft
{
public:
  Hovercraft();

protected:
  // Actual values in Earth-fixed coordinate frame
  double x_C, y_C;           // [m]        Position of CoM
  double x_P, y_P;           // [m]        Position of characteristic point P
  double x_C_p, y_C_p;       // [m/s]      Velocity of CoM
  double x_P_p, y_P_p;       // [m/s]      Velocity of P
  double psi;                // [rad]      Orientation
  double psi_p;              // [rad/s]    Angular velocity
  double z_r;                // [rad/s^2]  Disturbing angular acceleration

  // Actual values in Body-fixed coordinate frame
  double u_C, v_C;           // [m/s]      Velocity of CoM
  double z_u, z_v;           // [m/s^2]    Disturbing linear acceleration

  // Measured values
  double x_scn, y_scn;       // [m]        Position from scanner
  double x_p_scn, y_p_scn;   // [m/s]      Velocity estimated from scanner
  double psi_scn;            // [rad]      Orientation from scanner
  double psi_p_gyro;         // [rad/s]    Angular rate from gyroscope

  // Reference values in Earth-fixed coordinate frame
  double x_R, y_R;           // [m]        Position
  double psi_R;              // [rad]      Orientation
  double x_R_p, y_R_p;       // [m/s]      Linear velocity
  double psi_R_p;            // [rad/s]    Angular velocity
  double x_R_pp, y_R_pp;     // [m/s^2]    Linear acceleration
  double psi_R_pp;           // [rad/s^2]  Angular acceleration

  // Desired values for manual control
  double u_d;                // [m/s]      Surge
  double psi_p_d;            // [rad/s]    Angular velocity
  double psi_d;              // [rad]      Yaw

  // Control signals
  double u_1;                // [N]        Pushing
  double u_2;                // [N*m]      Orienting
  double u_3;                // [%]        Hover

  // Joystick signals
  double j_roll, j_pitch, j_throttle; // [-]

  // Miscellaneaous
  double t;                  // [s]        Time
};

} // namespace float_hc

#endif // HOVERCRAFT_H
