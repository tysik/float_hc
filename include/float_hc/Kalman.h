#ifndef KALMAN_H
#define KALMAN_H

#include <armadillo>

class KalmanFilter
{
public:
  KalmanFilter(uint dim_in, uint dim_out, uint dim_state);

private:
  // Dimensions:
  uint l;             // Input
  uint m;             // Output
  uint n;             // State

public:
  // System matrices:
  arma::mat A;       // State
  arma::mat B;       // Input
  arma::mat C;       // Output

  // Covariance matrices:
  arma::mat Q;       // Process
  arma::mat R;       // Measurement
  arma::mat P;       // Estimate error

  // Kalman gain matrix
  arma::mat K;

  // Vectors
  arma::vec u;       // Input
  arma::vec q_pred;  // Predicted state
  arma::vec q_est;   // Estimated state
  arma::vec y;       // Measurement

private:
  void initializeMatrices();

public:
  void updateState();
};

#endif // KALMAN_H
