#ifndef FLOAT_STATE_ESTIMATOR_H
#define FLOAT_STATE_ESTIMATOR_H

#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Quaternion.h>
#include <tf/transform_broadcaster.h>

#include "float_hc/Controls.h"
#include "float_hc/State.h"
#include "float_hc/Params.h"
#include "float_hc/Kalman.h"
#include "float_hc/Hovercraft.h"
#include "float_hc/float_defines.h"
#include "float_hc/float_inlines.h"

namespace float_hc
{

class FloatStateEstimator : public Hovercraft
{
public:
  FloatStateEstimator(ros::NodeHandle n);
  ~FloatStateEstimator();

public:
  bool estimator_started;

private:
  ros::NodeHandle nh;

  ros::Subscriber controls_sub, imu_sub, pose2D_sub;
  ros::Publisher state_pub;
  ros::ServiceServer params_srv;

  float_hc::State state_msg;

  tf::TransformBroadcaster tf_br;
  tf::StampedTransform state_tf;

  // Kalman filters
  KalmanFilter *AK;   // Angular KF
  KalmanFilter *LK;   // Linear KF

  double u_1_prev, u_2_prev;  // Previous samples of control signals

public:
  void estimateState();
  void publishStateMessage();
  void publishStateTransform();

private:
  bool paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res);
  void imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg);
  void pose2DCallback(const geometry_msgs::Pose2D::ConstPtr& pose2D_msg);
  void controlsCallback(const float_hc::Controls::ConstPtr& controls_msg);

  void initializeAngularKalman();
  void initializeLinearKalman();
};

} // namespace float_hc

#endif // FLOAT_STATE_ESTIMATOR_H
