#ifndef FLOAT_CONTROLLER_H
#define FLOAT_CONTROLLER_H

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>

#include "float_hc/State.h"
#include "float_hc/Controls.h"
#include "float_hc/Reference.h"
#include "float_hc/Desired.h"
#include "float_hc/Params.h"
#include "float_hc/Hovercraft.h"
#include "float_hc/float_defines.h"
#include "float_hc/float_inlines.h"

/*
 * Komentarz testowy 22222
 */

namespace float_hc
{

class FloatController : public Hovercraft
{
public:
  FloatController(ros::NodeHandle n);
  ~FloatController();

public:
  bool controller_started;
  bool controls_scaling;

private:
  ros::NodeHandle nh;
  ros::Publisher controls_pub;
  ros::Publisher desired_pub;
  ros::Subscriber joy_sub;
  ros::Subscriber state_sub;
  ros::Subscriber reference_sub;
  ros::ServiceServer params_srv;

  float_hc::Controls controls_msg;
  float_hc::Desired desired_msg;

  enum ControlType {CONSOLE = 1, MANUAL = 2, AUTOMATIC = 3} control_type;

  struct ManualControl
  {
    // Joystick gains
    double roll2rate, pitch2speed;   // [rad/s], [m/s]

    // Controller gains
    double k_p_u, k_d_u;             // [N*s/m], [N*s^2/m]
    double k_p_r, k_i_r, k_d_r;      // [N*m*s/rad], [N*m/rad], [N*m*s^2/rad]

    // Partial control signals
    double u_p_u, u_d_u;             // [N], [N]
    double u_p_r, u_i_r, u_d_r;      // [N*m], [N*m], [N*m]

    void initialize();
  } MC;

  struct AutomaticControl
  {
    // Controller gains
    double k_P, k_B_n, k_B_t;
    double epsilon;    // Convergence range [-]
    double throttle;   // Hover control [%]

    // Variables in {E}
    double F_x, F_y;       // [N]   Resultant force
    double F_P_x, F_P_y;   // [N]   Potential force
    double F_B_x, F_B_y;   // [N]   Braking force
    double F_I_x, F_I_y;   // [N]   Inertial force
    double F_V_x, F_V_y;   // [N]   Inner force
    double F_O_x, F_O_y;   // [N]   Outer force
    double x_RP, y_RP;     // [m]   Relative position (position error)
    double x_RP_p, y_RP_p; // [m/s] Relative velocity (velocity error)

    // Variables in {T}
    double F_n, F_t;       // [N]   Resultant force
    double F_B_n, F_B_t;   // [N]   Braking force
    double E_K_n, E_K_t;   // [J]   Relative kinetic energy
    double n_RP_p, t_RP_p; // [m/s] Relative velocity

    // Miscellaneous
    double F_c;            // [N]   Centripetal force
    double norm_RP;        // [m]   Relative distance
    double phi_RP;         // [rad] Auxiliary orientation

    double dot_VR, cross_VR;  // [m^2/s]  Auxiliary vector multiplications
    double dot_FR, cross_FR;  // [N*m]    Auxiliary vector multiplications
    double sin_psi, cos_psi;  // [-]      Trig functions macro

    void initialize();
    void calculatePotentialForce() { F_P_x = -k_P*x_RP;
                                     F_P_y = -k_P*y_RP; }
    void calculateBrakingForce();
    void calculateInertialForce(double x_R_pp, double y_R_pp) { F_I_x = MASS*x_R_pp;
                                                                F_I_y = MASS*y_R_pp; }
    void calculateInnerForce(double u_C, double v_C, double psi_p);
    void calculateOuterForce(double z_u, double z_v, double z_r);
    void calculateResultantForce() { F_x = F_P_x + F_B_x + F_I_x + F_V_x + F_O_x;
                                     F_y = F_P_y + F_B_y + F_I_y + F_V_y + F_O_y; }
  } AC;

public:
  void computeControls();
  void publishControlsMessage();
  void publishDesiredMessage();

private:
  bool paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res);
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);
  void stateCallback(const float_hc::State::ConstPtr& state_msg);
  void referenceCallback(const float_hc::Reference::ConstPtr& reference_msg);

  void computeManualControls();
  void computeAutomaticControls();
  void scaleControlSignals();

  void initializeManualController();
  void initializeAutomaticController();
};

} // namespace float_hc

#endif // FLOAT_CONTROLLER_H
