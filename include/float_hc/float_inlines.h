#ifndef FLOAT_INLINES_H
#define FLOAT_INLINES_H

#include <cmath>

inline double lowPassFilter(double x, double prev_x_f, double Tp, double Tf)
    {return prev_x_f + Tp/(Tp + Tf)*(x - prev_x_f);}

inline double lowPassFilteredDerivative(double x, double prev_x, double prev_x_p_f, double Tp, double Tf)
    {return (x - prev_x + prev_x_p_f*Tf)/(Tf + Tp);}

inline int signum(double f)
  {if (f<0) return -1; else return 1;}

inline double absolute(double f)
  {if (f<0) return -f; else return f;}

inline double continuousAngle(double psi, double prev_psi)
{
  double delta_psi = psi - atan2(sin(prev_psi), cos(prev_psi));
  if (delta_psi > M_PI)
    delta_psi -= 2*M_PI;
  else if (delta_psi < -M_PI)
    delta_psi += 2*M_PI;
  return prev_psi + delta_psi;
}

#endif
