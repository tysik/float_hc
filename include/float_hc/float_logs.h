#ifndef FLOAT_LOGS_H
#define FLOAT_LOGS_H

#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Pose2D.h>
#include <fstream>
#include <string>
#include <ctime>
#include "float_hc/Hovercraft.h"
#include "float_hc/Params.h"
#include "float_hc/State.h"
#include "float_hc/Desired.h"
#include "float_hc/Controls.h"
#include "float_hc/Reference.h"
#include "float_hc/float_defines.h"
#include "float_hc/float_inlines.h"

namespace float_hc
{

class FloatLogs : public Hovercraft
{

public:
  FloatLogs(ros::NodeHandle n);
  ~FloatLogs();

public:
  bool registering_data;

private:
  ros::NodeHandle nh;

  ros::Subscriber joy_sub;
  ros::Subscriber imu_sub;
  ros::Subscriber state_sub;
  ros::Subscriber desired_sub;
  ros::Subscriber controls_sub;
  ros::Subscriber reference_sub;
  ros::Subscriber pose2D_sub;
  ros::ServiceServer params_srv;

  // Data logging variables
  std::fstream file;
  std::string file_name, user_name, date;
  int num_sample;

public:
  void saveDataToFile();

private:
  bool paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res);
  void joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg);
  void imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg);
  void stateCallback(const float_hc::State::ConstPtr& state_msg);
  void desiredCallback(const float_hc::Desired::ConstPtr& desired_msg);
  void controlsCallback(const float_hc::Controls::ConstPtr& controls_msg);
  void referenceCallback(const float_hc::Reference::ConstPtr& reference_msg);
  void pose2DCallback(const geometry_msgs::Pose2D::ConstPtr& pose2D_msg);

  void prepareLogsFile();
};

} // namespace float_hc

#endif // FLOAT_LOGS_H
