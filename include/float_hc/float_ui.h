#ifndef FLOAT_UI_H
#define FLOAT_UI_H

#include <ros/ros.h>
#include <boost/lexical_cast.hpp>
#include <sys/types.h>
#include <signal.h>
#include <iostream>
#include <string>
#include <map>

#include "float_hc/Params.h"
#include "float_hc/float_defines.h"

namespace float_hc
{

class FloatUI
{
public:
  FloatUI(ros::NodeHandle n);
  ~FloatUI();

private:
  ros::NodeHandle nh;
  ros::ServiceClient controller_client;
  ros::ServiceClient estimator_client;
  ros::ServiceClient reference_client;
  ros::ServiceClient stm_client;
  ros::ServiceClient logs_client;

  typedef std::pair<std::string, double*> ParamPair;
  typedef std::map<int, std::string> MenuMap;
  typedef std::map<int, ParamPair > ParamMap;

  MenuMap main_options;
  ParamMap controller_options;
  ParamMap estimator_options;
  ParamMap reference_options;
  ParamMap controls_options;

  // Display state
  enum DisplayState {MAIN = 1, CONTROLLER = 2, ESTIMATOR = 3, REFERENCE = 4, CONTROLS = 5} display_type;
  enum ControlType {CONSOLE = 1, MANUAL = 2, AUTOMATIC = 3} control_type;
  enum ReferenceType {POINT = 1, LINEAR = 2, LISSAJOUS = 3} reference_type;

  // Conditional variables
  bool registering_data;
  bool controller_started;
  bool estimator_started;
  bool reference_started;
  bool controls_scaling;

  // Container for temporary data
  float_hc::Params params_msg;
  std::string s_option, s_value;
  int option;
  double value;

  // Controller parameters
  double roll2rate, pitch2speed;
  double k_p_u, k_d_u, k_p_r, k_i_r, k_d_r;
  double k_P, k_B_n, k_B_t, ac_throttle;

  // Control signals
  double u_1, u_2, u_3;

  // Estimator parameters
  double psi_var, psi_p_var, z_r_var, z_r_p_var;
  double pos_var, vel_var, acc_var;

  // Reference trajectory parameters
  double v, phi;
  double T, R_x, R_y;
  double n_x, n_y;

public:
  void displayUI();

private:
  void displayMainOptions();
  void displayControllerOptions();
  void displayEstimatorOptions();
  void displayReferenceOptions();
  void displayConsoleControls();
  void displayControlType();
  void displayReferenceType();

  void toggleDataRegistration();
  void resetSTM();
  void setSTM();

  void initializeVariables();
  void initializeMenus();
};

} // namespace float_hc

#endif // FLOAT_UI_H
