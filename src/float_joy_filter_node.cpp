#include "float_hc/float_joy_filter.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_joy_filter_node");
  ros::NodeHandle n;

  JoyFilter *JF = new JoyFilter(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
   JF->publishMessage();
   
    ros::spinOnce();
    loop_rate.sleep();
  }

  delete JF;

  return 0;
}
