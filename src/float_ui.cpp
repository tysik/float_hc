#include "float_hc/float_ui.h"

using namespace float_hc;

FloatUI::FloatUI(ros::NodeHandle n)
{
  controller_client = nh.serviceClient<float_hc::Params>("float_controller_params");
  estimator_client = nh.serviceClient<float_hc::Params>("float_state_estimator_params");
  reference_client = nh.serviceClient<float_hc::Params>("float_reference_params");
  stm_client = nh.serviceClient<float_hc::Params>("float_stm_params");
  logs_client = nh.serviceClient<float_hc::Params>("float_logs_params");

  initializeVariables();
  initializeMenus();

//  ROS_INFO("Awaiting controller");  while (!controller_client.call(params_msg));
//  ROS_INFO("Awaiting state estimator");  while (!estimator_client.call(params_msg));
//  ROS_INFO("Awaiting reference generator");  while (!reference_client.call(params_msg));
//  ROS_INFO("Awaiting STM");  while (!stm_client.call(params_msg));
//  ROS_INFO("Awaiting logs");  while (!logs_client.call(params_msg));

  ros::Duration(0.5).sleep();

  ROS_INFO("Float UI: start");
}


FloatUI::~FloatUI()
{
  ROS_INFO("Float UI: exit");
}


void FloatUI::initializeVariables()
{
  // Set display state to main window
  display_type = MAIN;
  control_type = static_cast<ControlType>(CONTROL_TYPE);
  reference_type = static_cast<ReferenceType>(REFERENCE_TYPE);

  controller_started = true;
  estimator_started = true;
  reference_started = true;
  registering_data = false;
  controls_scaling = true;

  // Controller parameters
  roll2rate = ROLL2RATE;
  pitch2speed = PITCH2SPEED;
  k_p_u = K_P_U;
  k_p_r = K_P_R;
  k_d_u = K_D_U;
  k_i_r = K_I_R;
  k_d_r = K_D_R;
  k_P = K_P;
  k_B_n = K_B;
  k_B_t = K_B;
  ac_throttle = AC_THROTTLE;

  // Estimator parameters
  psi_var = PSI_VAR;
  psi_p_var = PSI_P_VAR;
  z_r_var = Z_R_VAR;
  z_r_p_var = Z_R_P_VAR;

  pos_var = POS_VAR;
  vel_var = VEL_VAR;
  acc_var = ACC_VAR;

  // Control signals
  u_1 = u_2 = u_3 = 0.0;

  // Reference trajectory parameters
  v = LIN_V;
  phi = LIN_PHI;
  T = LIS_T;
  R_x = LIS_R_X;
  R_y = LIS_R_Y;
  n_x = n_y = 1;
}


void FloatUI::initializeMenus()
{
  int i = 1;
  main_options.insert(std::pair<int, std::string>(i++, "Controller options"));
  main_options.insert(std::pair<int, std::string>(i++, "State estimator options"));
  main_options.insert(std::pair<int, std::string>(i++, "Reference trajectory options"));
  main_options.insert(std::pair<int, std::string>(i++, "Console controls"));
  main_options.insert(std::pair<int, std::string>(i++, "Reset STM"));
  main_options.insert(std::pair<int, std::string>(i++, "Set STM"));

  i = 1;
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Toggle Controller", NULL)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Toggle Controls Scaling", NULL)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Controller Type", NULL)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("roll2rate", &roll2rate)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("pitch2speed", &pitch2speed)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_p_u", &k_p_u)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_d_u", &k_d_u)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_p_r", &k_p_r)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_i_r", &k_i_r)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_d_r", &k_d_r)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_P", &k_P)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_B_t", &k_B_t)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("k_B_n", &k_B_n)));
  controller_options.insert(std::pair<int, ParamPair>(i++, ParamPair("AC_Throttle", &ac_throttle)));

  i = 1;
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Toggle State Estimator", NULL)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("psi_var", &psi_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("psi_p_var", &psi_p_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("z_r_var", &z_r_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("z_r_p_var", &z_r_p_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("pos_var", &pos_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("vel_var", &vel_var)));
  estimator_options.insert(std::pair<int, ParamPair>(i++, ParamPair("acc_var", &acc_var)));

  i = 1;
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Toggle Reference Generator", NULL)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Reference Type", NULL)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("v", &v)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("phi", &phi)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("T", &T)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("R_x", &R_x)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("R_y", &R_y)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("n_x", &n_x)));
  reference_options.insert(std::pair<int, ParamPair>(i++, ParamPair("n_y", &n_y)));

  i = 1;
  controls_options.insert(std::pair<int, ParamPair>(i++, ParamPair("Zero all signals", NULL)));
  controls_options.insert(std::pair<int, ParamPair>(i++, ParamPair("u_1", &u_1)));
  controls_options.insert(std::pair<int, ParamPair>(i++, ParamPair("u_2", &u_2)));
  controls_options.insert(std::pair<int, ParamPair>(i++, ParamPair("u_3", &u_3)));
}


void FloatUI::displayUI()
{
  while (ros::ok())
  {
    std::system("clear");
    std::cout << "*************[FLOAT User Interface (v1.5)]*************" 	<< std::endl;
    std::cout << "["<< (registering_data ? "R" : "-") << "] (Type 'R' to toggle recording)" << std::endl;

    switch (display_type)
    {
      case MAIN:        { displayMainOptions(); break; }
      case CONTROLLER:  { displayControllerOptions(); break; }
      case ESTIMATOR:   { displayEstimatorOptions(); break; }
      case REFERENCE:   { displayReferenceOptions(); break; }
      case CONTROLS:    { displayConsoleControls(); break; }
    }

    std::cout << "Choose an option: ";
    std::cin >> s_option;
    if (s_option == "R") { toggleDataRegistration(); continue; }

    try {option = boost::lexical_cast<int>(s_option);}
    catch ( const boost::bad_lexical_cast &exc ) {continue;}
    params_msg.request.option = option;

    switch (display_type)
    {
      case MAIN:
        switch (option)
        {
          case 0:   { system("clear"); ros::shutdown(); }
          case 1:   { display_type = CONTROLLER; continue; }
          case 2:   { display_type = ESTIMATOR; continue; }
          case 3:   { display_type = REFERENCE; continue; }
          case 4:   { display_type = CONTROLS; continue; }
          case 5:   { resetSTM(); continue; }
          case 6:   { setSTM(); continue; }
          default:  { continue; }
        }
      break;

      case CONTROLLER:
        if (option == 0) { display_type = MAIN; continue; }
        else if (option < 0 || option > controller_options.size()) continue;
        else std::cout << "[" << controller_options[option].first << "] ";
      break;

      case ESTIMATOR:
        if (option == 0) { display_type = MAIN; continue; }
        else if (option < 0 || option > estimator_options.size()) continue;
        else std::cout << "[" << estimator_options[option].first << "] ";
      break;

      case REFERENCE:
        if (option == 0) { display_type = MAIN; continue; }
        else if (option < 0 || option > reference_options.size()) continue;
        else std::cout << "[" << reference_options[option].first << "] ";
      break;

      case CONTROLS:
        if (option == 0) { display_type = MAIN; continue; }
        else if (option < 0 || option > controls_options.size()) continue;
        else std::cout << "[" << controls_options[option].first << "] ";
      break;
    }

    std::cout << "Enter value: ";
    std::cin >> s_value;
    try {value = boost::lexical_cast<float>(s_value);}
    catch ( const boost::bad_lexical_cast &exc ) {continue;}
    params_msg.request.value = value;

    if (display_type == CONTROLLER)
    {
      if (controller_client.call(params_msg))
      {
        if (option == 1) controller_started = (value != 0);
        else if (option == 2) controls_scaling = (value != 0);
        else if (option == 3)
        {
          if (value == 1) control_type = CONSOLE;
          else if (value == 2) control_type = MANUAL;
          else if (value == 3) control_type = AUTOMATIC;
        }
        else *controller_options[option].second = value;
      }
      else ROS_INFO("Could not connect to float_controller_parameters server.");
    }

    else if (display_type == ESTIMATOR)
    {
      if (estimator_client.call(params_msg))
      {
        if (option == 1) estimator_started = (value != 0);
        else *estimator_options[option].second = value;
      }
      else ROS_INFO("Could not connect to float_state_estimator_parameters server.");
    }

    else if (display_type == REFERENCE)
    {
      if (reference_client.call(params_msg))
      {
        if (option == 1) reference_started = (value != 0);
        else if (option == 2)
        {
          if (value == 1) reference_type = POINT;
          else if (value == 2) reference_type = LINEAR;
          else if (value == 3) reference_type = LISSAJOUS;
        }
        else *reference_options[option].second = value;
      }
      else ROS_INFO("Could not connect to float_reference_parameters server.");
    }

    else if (display_type == CONTROLS)
    {
      if (control_type == CONSOLE)
      {
        params_msg.request.option = option + 19; // Offset options by 19 (u_1->21, u_2->22, etc.)
        if (controller_client.call(params_msg))
        {
          if (option == 1) {u_1 = u_2 = u_3 = 0.0;}
          else *controls_options[option].second = value;
        }
        else ROS_INFO("Could not connect to float_controller_parameters server.");
      }
      else ROS_INFO("Control type not set to CONSOLE.");

      if (u_1 > MAX_THRUST) u_1 = MAX_THRUST;
      else if (u_1 < 0.0) u_1 = 0.0;

      if (u_2 > MAX_TORQUE) u_2 = MAX_TORQUE;
      else if (u_2 < -MAX_TORQUE) u_2 = -MAX_TORQUE;

      if (u_3 > MAX_THROTTLE) u_3 = MAX_THROTTLE;
      else if (u_3 < 0.0) u_3 = 0.0;
    }
  }
}


void FloatUI::displayMainOptions()
{
  std::cout << "" << std::endl;
  std::cout << "Controller Type: "; displayControlType();
  std::cout << ", " << (controller_started ? "ACTIVE" : "STOPPED") << std::endl;
  std::cout << "Trajectory Type: "; displayReferenceType();
  std::cout << ", " << (reference_started ? "ACTIVE" : "STOPPED") << std::endl;
  std::cout << "-------------------------------------------------------" << std::endl;

  for (int i=1; i<=main_options.size(); i++)
    std::cout << i << ". " << main_options[i] << std::endl;

  std::cout << "" << std::endl;
  std::cout << "0. EXIT" << std::endl;
  std::cout << "*******************************************************" << std::endl;
}

void FloatUI::displayControllerOptions()
{
  std::cout << "Controller Type: "; displayControlType();
  std::cout << ", " << (controller_started ? "ACTIVE" : "STOPPED")
            << ", " << (controls_scaling ? "WITH SCALING" : "W/O SCALING") << std::endl;
  std::cout << "kpu=" << k_p_u << ", kdu=" << k_d_u << ", kpr=" << k_p_r << ", kir=" << k_i_r  << ", kdr=" << k_d_r << std::endl;
  std::cout << "kP=" << k_P << ", kBt=" << k_B_t << ", kBn=" << k_B_n << ", acThrottle=" << ac_throttle;
  std::cout << ", r2r=" << roll2rate << ", p2s=" << pitch2speed << std::endl;
  std::cout << "-------------------------------------------------------" 	<< std::endl;

  for (int i=1; i<=controller_options.size(); i++)
    std::cout << i << ". " << controller_options[i].first << std::endl;

  std::cout << "" << std::endl;
  std::cout << "0. BACK" << std::endl;
  std::cout << "*******************************************************" << std::endl;
}

void FloatUI::displayEstimatorOptions()
{
  std::cout << "" << std::endl;
  std::cout << "psi=" << psi_var << ", psi_p=" << psi_p_var << ", z_r=" << z_r_var << ", z_r_p=" << z_r_p_var << std::endl;
  std::cout << "pos=" << pos_var << ", vel=" << vel_var << ", acc=" << acc_var << std::endl;
  std::cout << "-------------------------------------------------------" << std::endl;

  for (int i=1; i<=estimator_options.size(); i++)
    std::cout << i << ". " << estimator_options[i].first << std::endl;

  std::cout << "" << std::endl;
  std::cout << "0. BACK" << std::endl;
  std::cout << "*******************************************************" << std::endl;
}


void FloatUI::displayReferenceOptions()
{
  std::cout << "Trajectory Type: "; displayReferenceType();
  std::cout << ", "<< (reference_started ? "ACTIVE" : "STOPPED") << std::endl;
  std::cout << "v=" << v << ", phi=" << phi << std::endl;
  std::cout << "T=" << T << ", R_X=" << R_x << ", R_Y=" << R_y << ", n_x=" << n_x << ", n_y=" << n_y << std::endl;
  std::cout << "-------------------------------------------------------" << std::endl;

  for (int i=1; i<=reference_options.size(); i++)
    std::cout << i << ". " << reference_options[i].first << std::endl;

  std::cout << "" << std::endl;
  std::cout << "0. BACK" << std::endl;
  std::cout << "*******************************************************" << std::endl;
}


void FloatUI::displayConsoleControls()
{
  std::cout << "u1 = " << u_1 << std::endl;
  std::cout << "u2 = " << u_2 << std::endl;
  std::cout << "u3 = " << u_3 << std::endl;
  std::cout << "-------------------------------------------------------" << std::endl;

  for (int i=1; i<=controls_options.size(); i++)
    std::cout << i << ". " << controls_options[i].first << std::endl;

  std::cout << "" << std::endl;
  std::cout << "0. BACK" << std::endl;
  std::cout << "*******************************************************" << std::endl;
}


void FloatUI::toggleDataRegistration()
{
  params_msg.request.option = 1;
  if (registering_data) params_msg.request.value = 0.0;
  else params_msg.request.value = 1.0;

  if (logs_client.call(params_msg)) registering_data = !registering_data;
  else ROS_INFO("Could not toggle data registration.");
}


void FloatUI::resetSTM()
{
  params_msg.request.option = 2;
  params_msg.request.value = 1.0;
  if (stm_client.call(params_msg)) ROS_INFO("STM reset.");
  else ROS_INFO("Could not reset the STM.");

  ros::Duration(0.05).sleep();

  params_msg.request.option = 2;
  params_msg.request.value = 0.0;
  stm_client.call(params_msg);
}


void FloatUI::setSTM()
{
  params_msg.request.option = 1;
  params_msg.request.value = 1.0;
  if (stm_client.call(params_msg)) ROS_INFO("STM set.");
  else ROS_INFO("Could not set the STM.");

  ros::Duration(0.05).sleep();

  params_msg.request.option = 1;
  params_msg.request.value = 0.0;
  stm_client.call(params_msg);
}


void FloatUI::displayControlType()
{
  if (control_type == CONSOLE) std::cout << "CONSOLE";
  else if (control_type == MANUAL) std::cout << "MANUAL";
  else if (control_type == AUTOMATIC) std::cout << "AUTOMATIC";
}


void FloatUI::displayReferenceType()
{
  if (reference_type == POINT) std::cout << "POINT";
  else if (reference_type == LINEAR) std::cout << "LINEAR";
  else if (reference_type == LISSAJOUS) std::cout << "LISSAJOUS";
}
