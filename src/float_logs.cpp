#include "float_hc/float_logs.h"

using namespace float_hc;

FloatLogs::FloatLogs(ros::NodeHandle n) : nh(n)
{
  joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 5, &FloatLogs::joyCallback, this);
  imu_sub = nh.subscribe<sensor_msgs::Imu>("float/imu", 5, &FloatLogs::imuCallback, this);
  state_sub = nh.subscribe<float_hc::State>("float/state", 5, &FloatLogs::stateCallback, this);
  controls_sub = nh.subscribe<float_hc::Controls>("float/controls", 5, &FloatLogs::controlsCallback, this);
  reference_sub = nh.subscribe<float_hc::Reference>("float/reference", 5, &FloatLogs::referenceCallback, this);
  pose2D_sub = nh.subscribe<geometry_msgs::Pose2D>("pose2D", 5, &FloatLogs::pose2DCallback, this);
  params_srv = nh.advertiseService("float_logs_params", &FloatLogs::paramsCallback, this);

  registering_data = false;

  // Get the username
  char *login = getenv("USER");
  user_name = std::string(login);

  ROS_INFO("Float logs: start");
}


FloatLogs::~FloatLogs()
{
  if (file.is_open()) file.close();
  ROS_INFO("Float logs: exit");
}


void FloatLogs::joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
{
  j_roll     = joy_msg->axes[0];
  j_pitch    = joy_msg->axes[1];
  j_throttle = joy_msg->axes[2];
}


void FloatLogs::desiredCallback(const float_hc::Desired::ConstPtr& desired_msg)
{
  psi_d = desired_msg->psi_d;
  psi_p_d = desired_msg->psi_p_d;
  u_d = desired_msg->u_d;
}


void FloatLogs::imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg)
{
  psi_p_gyro = imu_msg->angular_velocity.z - PSI_P_GYRO_OFFSET;
}


void FloatLogs::controlsCallback(const float_hc::Controls::ConstPtr& controls_msg)
{
  u_1 = controls_msg->u_1;
  u_2 = controls_msg->u_2;
  u_3 = controls_msg->u_3;
}


void FloatLogs::referenceCallback(const float_hc::Reference::ConstPtr& reference_msg)
{
  x_R   = reference_msg->x_r;
  y_R   = reference_msg->y_r;
  psi_R = reference_msg->psi_r;

  x_R_p   = reference_msg->x_r_p;
  y_R_p   = reference_msg->y_r_p;
  psi_R_p = reference_msg->psi_r_p;

  x_R_pp   = reference_msg->x_r_pp;
  y_R_pp   = reference_msg->y_r_pp;
  psi_R_pp = reference_msg->psi_r_pp;

  t = reference_msg->t;
}


void FloatLogs::pose2DCallback(const geometry_msgs::Pose2D::ConstPtr& pose2D_msg)
{
  x_p_scn = (pose2D_msg->x - x_scn)/TP_SCN;
  y_p_scn = (pose2D_msg->y - y_scn)/TP_SCN;

  x_scn = pose2D_msg->x;
  y_scn = pose2D_msg->y;
  psi_scn = continuousAngle(pose2D_msg->theta, psi_scn);
}


void FloatLogs::stateCallback(const float_hc::State::ConstPtr& state_msg)
{
  x_C = state_msg->x;
  y_C = state_msg->y;
  psi = state_msg->psi;

  u_C   = state_msg->u;
  v_C   = state_msg->v;
  psi_p = state_msg->psi_p;

  z_u = state_msg->z_u;
  z_v = state_msg->z_v;
  z_r = state_msg->z_r;
}


bool FloatLogs::paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res)
{
  if (req.option == 1)
  {
    if (req.value != 0)
    {
      prepareLogsFile();
      ROS_INFO("Started data registration");
      registering_data = true;
    }
    else
    {
      registering_data = false;
      ROS_INFO("Stopped data registration");
    }
  }

  ROS_INFO("Change: [%d][%f]", req.option, req.value);

  return true;
}


void FloatLogs::prepareLogsFile()
{
  // Get the date for the filename
  time_t now = time(NULL);
  char the_date[30];
  the_date[0] = '\0';

  if (now != -1)
    strftime(the_date, 30, "%Y_%m_%d_%H_%M_%S", gmtime(&now));
  date = std::string(the_date);

  file_name = "/home/" + user_name + "/Float/float_data_" + date +".txt";

  file.open((char*)file_name.c_str(), std::ios::out | std::ios::app);
  if (file.good())
    ROS_INFO("Log file: File opened");
  else
    ROS_WARN("Log file: Couldn't open file");

  // Write header to file
  file << "n \t x_C \t y_C \t psi \t u_C \t v_C \t psi_p \t z_r \t z_u \t z_v \t "
       << "x_scn \t y_scn \t psi_scn \t x_p_scn \t y_p_scn \t psi_p_gyro \t "
       << "x_R \t y_R \t psi_R \t x_R_p \t y_R_p \t psi_R_p \t "
       << "u_1 \t u_2 \t u_3 \t j_roll \t j_pitch \t j_throttle \t u_d \t psi_d \t psi_p_d \t t \n";
  file.close();

  num_sample = 0;
}


void FloatLogs::saveDataToFile()
{
  num_sample++;

  file.open((char*)file_name.c_str(), std::ios::out | std::ios::app);
  file  << num_sample << "\t" << x_C << "\t" << y_C << "\t" << psi << "\t"
        << u_C << "\t" << v_C << "\t" << psi_p << "\t"
        << z_u << "\t" << z_v << "\t" << z_r << "\t"
        << x_scn << "\t" << y_scn << "\t" << psi_scn << "\t"
        << x_p_scn << "\t" << y_p_scn << "\t" << psi_p_gyro << "\t"
        << x_R << "\t" << y_R << "\t" << psi_R << "\t"
        << x_R_p << "\t" << y_R_p << "\t" << psi_R_p << "\t"
        << u_1 << "\t" << u_2 << "\t" << u_3 << "\t"
        << j_roll << "\t" << j_pitch << "\t" << j_throttle << "\t"
        << u_d << "\t" << psi_d << "\t" << psi_p_d << "\t"
        << t << "\n";
  file.close();
}
