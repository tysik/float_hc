#include "float_hc/float_state_estimator.h"

using namespace float_hc;

FloatStateEstimator::FloatStateEstimator(ros::NodeHandle n) : nh(n)
{
  state_pub = nh.advertise<float_hc::State>("float/state", 5);
  imu_sub = nh.subscribe<sensor_msgs::Imu>("float/imu", 5, &FloatStateEstimator::imuCallback, this);
  pose2D_sub = nh.subscribe<geometry_msgs::Pose2D>("pose2D", 5, &FloatStateEstimator::pose2DCallback, this);
  controls_sub = nh.subscribe<float_hc::Controls>("float/controls", 5, &FloatStateEstimator::controlsCallback, this);
  params_srv = nh.advertiseService("float_state_estimator_params", &FloatStateEstimator::paramsCallback, this);

  estimator_started = true;

  u_1_prev = u_2_prev = 0.0;

  initializeAngularKalman();
  initializeLinearKalman();

  ROS_INFO("Float state estimator: start");
}


FloatStateEstimator::~FloatStateEstimator()
{
  ROS_INFO("Float state estimator: exit");
}


void FloatStateEstimator::initializeAngularKalman()
{
  // Input: [u_2],  Output: [psi_scn, psi_p_gyro],  State: [psi, psi_p, z_r, z_r_p]
  AK = new KalmanFilter(1, 2, 4);

  AK->A << 1.0 << TP  << TP*TP/2 << TP*TP*TP/6 << arma::endr  // psi(k+1)
        << 0.0 << 1.0 << TP      << TP*TP/2    << arma::endr  // psi_p(k+1)
        << 0.0 << 0.0 << 1.0     << TP         << arma::endr  // z_r(k+1)
        << 0.0 << 0.0 << 0.0     << 1.0        << arma::endr; // z_r_p(k+1)

  AK->B << TP*TP/(2*INERTIA) << arma::endr
        << TP/INERTIA        << arma::endr
        << 0.0               << arma::endr
        << 0.0               << arma::endr;


  AK->C << 1.0 << 0.0 << 0.0 << 0.0 << arma::endr
        << 0.0 << 1.0 << 0.0 << 0.0 << arma::endr;

  AK->Q(0,0) = PSI_VAR;  AK->Q(1,1) = PSI_P_VAR;
  AK->Q(2,2) = Z_R_VAR;  AK->Q(3,3) = Z_R_P_VAR;

  AK->R(0,0) = PSI_SCN_VAR;  AK->R(1,1) = GYRO_VAR;
}


void FloatStateEstimator::initializeLinearKalman()
{
  // Input [u_1],  Output [x_scn, u_scn, y_scn, v_scn],  State  [x, u, z_u, y, v, z_u] (Centre of Mass)
  LK = new KalmanFilter(1, 4, 6);

  LK->A << 1.0 << TP*cos(psi) << 0.5*TP*TP*cos(psi) << 0.0 << -TP*sin(psi) << -0.5*TP*TP*sin(psi) << arma::endr  // x(k+1)
        << 0.0 << 1.0         << TP                 << 0.0 << TP*psi_p     << 0.0                 << arma::endr  // u(k+1)
        << 0.0 << 0.0         << 1.0                << 0.0 << 0.0          << 0.0                 << arma::endr  // u_d(k+1)
        << 0.0 << TP*sin(psi) << 0.5*TP*TP*sin(psi) << 1.0 << TP*cos(psi)  << 0.5*TP*TP*cos(psi)  << arma::endr  // y(k+1)
        << 0.0 << -TP*psi_p   << 0.0                << 0.0 << 1.0          << TP                  << arma::endr  // v(k+1)
        << 0.0 << 0.0         << 0.0                << 0.0 << 0.0          << 1.0                 << arma::endr; // v_d(k+1)

  LK->B << 0.5*TP*TP*cos(psi)/MASS << arma::endr
        << TP/MASS                 << arma::endr
        << 0.0                     << arma::endr
        << 0.5*TP*TP*sin(psi)/MASS << arma::endr
        << 0.0                     << arma::endr
        << 0.0                     << arma::endr;

  LK->C << 1.0 << 0.0 << 0.0 << 0.0 << 0.0 << 0.0 << arma::endr
        << 0.0 << 1.0 << 0.0 << 0.0 << 0.0 << 0.0 << arma::endr
        << 0.0 << 0.0 << 0.0 << 1.0 << 0.0 << 0.0 << arma::endr
        << 0.0 << 0.0 << 0.0 << 0.0 << 1.0 << 0.0 << arma::endr;

  LK->Q(0,0) = POS_VAR;       LK->Q(1,1) = VEL_VAR;     LK->Q(2,2) = ACC_VAR;
  LK->Q(3,3) = POS_VAR;       LK->Q(4,4) = VEL_VAR;     LK->Q(5,5) = ACC_VAR;

  LK->R(0,0) = POS_SCN_VAR;   LK->R(1,1) = VEL_SCN_VAR;
  LK->R(2,2) = POS_SCN_VAR;   LK->R(3,3) = VEL_SCN_VAR;

  LK->P = LK->P;
}


void FloatStateEstimator::estimateState()
{
  // Angular Kalman Filter
  // Update input
  AK->u(0) = u_2_prev;

  // Update output
  AK->y(0) = psi_scn;
  AK->y(1) = psi_p_gyro;

  AK->updateState();

  psi = AK->q_est(0);
  psi_p = AK->q_est(1);
  z_r = AK->q_est(2);

  double cos_psi = cos(psi);
  double sin_psi = sin(psi);

  // Linear Kalman Filter
  // Transform velocity from {P} to {B}
  x_C_p = x_p_scn + BASE_LENGTH*psi_p*sin_psi;
  y_C_p = y_p_scn - BASE_LENGTH*psi_p*cos_psi;

  // Update input
  LK->u(0) = u_1_prev;

  // Update output
  LK->y(0) = x_scn - BASE_LENGTH*cos_psi;
  LK->y(1) = x_C_p*cos_psi + y_C_p*sin_psi;
  LK->y(2) = y_scn - BASE_LENGTH*sin_psi;
  LK->y(3) = -x_C_p*sin_psi + y_C_p*cos_psi;

  // Update state and input matrix
  LK->A(0,1) = TP*cos_psi;  LK->A(0,2) = 0.5*TP*TP*cos_psi;  LK->A(0,4) = -TP*sin_psi;  LK->A(0,5) = -0.5*TP*TP*sin_psi;
  LK->A(1,4) = TP*psi_p;
  LK->A(3,1) = TP*sin_psi;  LK->A(3,2) = 0.5*TP*TP*sin_psi;  LK->A(3,4) = TP*cos_psi;   LK->A(3,5) = 0.5*TP*TP*cos_psi;
  LK->A(4,1) = -TP*psi_p;

  LK->B(0,0) = 0.5*TP*TP*cos_psi/MASS;
  LK->B(3,0) = 0.5*TP*TP*sin_psi/MASS;

  LK->updateState();

  x_C = LK->q_est(0);
  u_C = LK->q_est(1);
  z_u = LK->q_est(2);
  y_C = LK->q_est(3);
  v_C = LK->q_est(4);
  z_v = LK->q_est(5);
}


void FloatStateEstimator::publishStateMessage()
{
  state_msg.header.stamp = ros::Time::now();
  state_msg.x = x_C;
  state_msg.y = y_C;
  state_msg.psi = psi;

  state_msg.u = u_C;
  state_msg.v = v_C;
  state_msg.psi_p = psi_p;

  state_msg.z_u = z_u;
  state_msg.z_v = z_v;
  state_msg.z_r = z_r;

  state_pub.publish(state_msg);
}


void FloatStateEstimator::publishStateTransform()
{
  state_tf.setOrigin(tf::Vector3(x_C, y_C, 0.0));
  state_tf.setRotation(tf::createQuaternionFromYaw(psi));
  tf_br.sendTransform(tf::StampedTransform(state_tf, ros::Time::now(), "world", "float"));
}


void FloatStateEstimator::imuCallback(const sensor_msgs::Imu::ConstPtr& imu_msg)
{
  psi_p_gyro = imu_msg->angular_velocity.z - PSI_P_GYRO_OFFSET;
}


void FloatStateEstimator::pose2DCallback(const geometry_msgs::Pose2D::ConstPtr& pose2D_msg)
{
  x_p_scn = (pose2D_msg->x - x_scn)/TP_SCN;
  y_p_scn = (pose2D_msg->y - y_scn)/TP_SCN;

  x_scn = pose2D_msg->x;
  y_scn = pose2D_msg->y;
  psi_scn = continuousAngle(pose2D_msg->theta, psi_scn);
}


void FloatStateEstimator::controlsCallback(const float_hc::Controls::ConstPtr &controls_msg)
{
  double F_r, F_l;

  u_1_prev = u_1;
  u_2_prev = u_2;

  // Convert control signals to forces
  F_r = (controls_msg->u_1 + controls_msg->u_2/FORCE_ARM)/2;
  F_l = (controls_msg->u_1 - controls_msg->u_2/FORCE_ARM)/2;

  // Check if the control forces were realizable
  if (F_r < MIN_ENGINE_THRUST) F_r = 0.0;
  else if (F_r > MAX_ENGINE_THRUST) F_r = MAX_ENGINE_THRUST;

  if (F_l < MIN_ENGINE_THRUST) F_l = 0.0;
  else if (F_l > MAX_ENGINE_THRUST) F_l = MAX_ENGINE_THRUST;

  // Return to control signals
  u_1 = F_r + F_l;
  u_2 = FORCE_ARM*(F_r - F_l);
  u_3 = controls_msg->u_3;
}


bool FloatStateEstimator::paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res)
{
  switch (req.option)
  {
    case 1:
      estimator_started = (req.value != 0);
    break;

    case 2:
      if (req.value >= 0) AK->Q(0,0) = req.value;  // psi variance
    break;

    case 3:
      if (req.value >= 0) AK->Q(1,1) = req.value;  // psi_p variance
    break;

    case 4:
      if (req.value >= 0) AK->Q(2,2) = req.value;  // z_r variance
    break;

    case 5:
      if (req.value >= 0) AK->Q(3,3) = req.value;  // z_r_p variance
    break;

    case 6:
      if (req.value >= 0) { LK->Q(0,0) = req.value; LK->Q(3,3) = req.value; }  // pos variance
    break;

    case 7:
      if (req.value >= 0) { LK->Q(1,1) = req.value; LK->Q(4,4) = req.value; }  // vel variance
    break;

    case 8:
      if (req.value >= 0) { LK->Q(2,2) = req.value; LK->Q(5,5) = req.value; }  // acc variance
    break;
  }

  ROS_INFO("Change: [%d][%f]", req.option, req.value);

  return true;
}
