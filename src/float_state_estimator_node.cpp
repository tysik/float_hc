#include "float_hc/float_state_estimator.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_state_estimator_node");
  ros::NodeHandle n;

  FloatStateEstimator *FSE = new FloatStateEstimator(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
    if (FSE->estimator_started)
    {
      FSE->estimateState();
      FSE->publishStateMessage();
      FSE->publishStateTransform();
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  delete FSE;

  return 0;
}
