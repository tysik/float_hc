#include "float_hc/float_reference.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_reference_node");
  ros::NodeHandle n;

  FloatReference *FR = new FloatReference(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
    if (FR->reference_started)
    {
      FR->calculateReferenceTrajectory();
      FR->publishReferenceMessage();
      FR->publishReferenceTransform();
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  delete FR;

  return 0;
}
