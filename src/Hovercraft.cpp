#include "float_hc/Hovercraft.h"

using namespace float_hc;

Hovercraft::Hovercraft()
{
  // Initialize Float state
  x_C = -BASE_LENGTH;   // Scan matching sets initial position (0,0) and orientation (0) at the scanner tf.
  y_C = 0.0;
  x_C_p = y_C_p = u_C = v_C = 0.0;
  x_P = y_P = 0.0;
  x_P_p = y_P_p = 0.0;
  z_u = z_v = 0.0;
  psi = 0.0;
  psi_p = 0.0;
  z_r = 0.0;

  // Initialize reference state
  x_R = y_R = 0.0;
  psi_R = 0.0;
  x_R_p = y_R_p = 0.0;
  psi_R_p = 0.0;
  x_R_pp = y_R_pp = 0.0;
  psi_R_pp = 0.0;

  // Initialize desired state
  u_d = 0.0;
  psi_p_d = 0.0;
  psi_d = 0.0;

  // Initialize measured values
  x_scn = y_scn = 0.0;
  psi_scn = 0.0;
  x_p_scn = y_p_scn = 0.0;
  psi_p_gyro = 0.0;

  // Initialize control signals
  u_1 = u_2 = u_3 = 0.0;

  // Initialize joystick signals
  j_roll = j_pitch = 0.0;
  j_throttle = -1.0;

  // Initialize miscellaneous
  t = 0.0;
}
