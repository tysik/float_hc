#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include "float_hc/float_joy_filter.h"
using namespace float_hc;

JoyFilter::JoyFilter(ros::NodeHandle n) : nh(n)
{
  joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 1, &JoyFilter::JoyCallback, this);
  // joy_pub = nh.advertise<float_hc::JoyValues>("float/joy_filtered", 1000);
 // params_srv = nh.advertiseService("float_controller_params", &FloatController::paramsCallback, this);

 ROS_INFO("Float joy filter: start");
}

JoyFilter::~JoyFilter()
{
  ROS_INFO("Float joy filter: exit");
}

void JoyFilter::JoyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
{
  j_roll     = joy_msg->axes[0];
  j_pitch    = joy_msg->axes[1];
  j_throttle = joy_msg->axes[2];
}

void JoyFilter::publishMessage()
{
 // joy_pub.publish(j_roll);
}






