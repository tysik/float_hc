#include "float_hc/float_controller.h"

using namespace float_hc;

FloatController::FloatController(ros::NodeHandle n) : nh(n)
{
  joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 1, &FloatController::joyCallback, this);
  state_sub = nh.subscribe<float_hc::State>("float/state", 1, &FloatController::stateCallback, this);
  reference_sub = nh.subscribe<float_hc::Reference>("float/reference", 1, &FloatController::referenceCallback, this);
  controls_pub = nh.advertise<float_hc::Controls>("float/controls", 5);
  desired_pub = nh.advertise<float_hc::Desired>("float/desired", 5);
  params_srv = nh.advertiseService("float_controller_params", &FloatController::paramsCallback, this);

  controller_started = true;
  controls_scaling = true;
  control_type = static_cast<ControlType>(CONTROL_TYPE);

  MC.initialize();
  AC.initialize();

  ROS_INFO("Float controller: start");
}


FloatController::~FloatController()
{
  ROS_INFO("Float controller: exit");
}


void FloatController::ManualControl::initialize()
{
  // Joystick gains
  roll2rate = ROLL2RATE;
  pitch2speed = PITCH2SPEED;

  // Controller gains
  k_p_u = K_P_U;   k_d_u = K_D_U;
  k_p_r = K_P_R;   k_i_r = K_I_R;   k_d_r = K_D_R;

  // Partial control signals
  u_p_u = u_d_u = 0.0;
  u_p_r = u_i_r = u_d_r = 0.0;
}


void FloatController::AutomaticControl::initialize()
{
  // Controller gains
  k_P = K_P;
  k_B_n = K_B;
  k_B_t = K_B;

  throttle = AC_THROTTLE;
  epsilon = 0.01;

  // Variables
  F_x = F_y = F_P_x = F_P_y = 0.0;
  F_B_x = F_B_y = F_I_x = F_I_y = 0.0;
  F_V_x = F_V_y = F_O_x = F_O_y = 0.0;
  x_RP = y_RP = 0.0;
  x_RP_p = y_RP_p = 0.0;

  F_n = F_t = F_B_n = F_B_t = 0.0;
  E_K_n = E_K_t = 0.0;
  n_RP_p = t_RP_p = 0.0;

  F_c = 0.0;
  norm_RP = 0.0;
  phi_RP = 0.0;

  dot_VR = cross_VR = 0.0;
  dot_FR = cross_FR = 0.0;
  sin_psi = 0.0;
  cos_psi = 1.0;
}


void FloatController::computeControls()
{
  switch (control_type)
  {
    case CONSOLE:
    break;

    case MANUAL:
      computeManualControls();
    break;

    case AUTOMATIC:
      computeAutomaticControls();
    break;
  }

  if (controls_scaling) scaleControlSignals();
}


void FloatController::computeManualControls()
{
  // Convert joystick values into desired velocities
  if (MC.k_i_r != 0)
    psi_p_d = lowPassFilter(MC.roll2rate*j_roll, psi_p_d, TP, MC.k_p_r/MC.k_i_r);
  else
    psi_p_d = lowPassFilter(MC.roll2rate*j_roll, psi_p_d, TP, T_F);

  // Integrate the desired angular velocity (with anti wind-up)
  if (u_2*(psi_p_d - psi_p) > 0.0 && (u_2 >= MAX_TORQUE/8 || u_2 <= -MAX_TORQUE/8))
    psi_d += 0.0;
  else
    psi_d += TP*psi_p_d;

  u_d = lowPassFilter(MC.pitch2speed*j_pitch, u_d, TP, T_F);
  if (u_d < 0.0) u_d = 0.0;

  // Angular velocity regulator
  MC.u_p_r = MC.k_p_r*(psi_p_d - psi_p);
  MC.u_i_r = MC.k_i_r*(psi_d - psi);
  MC.u_d_r = MC.k_d_r*(-z_r);

  // Linear velocity regulator
  MC.u_p_u = MC.k_p_u*(u_d - u_C);
  MC.u_d_u = MC.k_d_u*(-z_u);

  // Control signals
  u_1 = MC.u_p_u + MC.u_d_u;
  u_2 = MC.u_p_r + MC.u_i_r + MC.u_d_r;
  u_3 = 50.0*(j_throttle + 1.0)*AC.throttle/100.0;
}


void FloatController::computeAutomaticControls()
{
  AC.sin_psi = sin(psi);
  AC.cos_psi = cos(psi);

  // Calculate position and velocity of point P in {E}
  x_P = x_C + BASE_LENGTH*AC.cos_psi;
  y_P = y_C + BASE_LENGTH*AC.sin_psi;
  x_C_p = u_C*AC.cos_psi - v_C*AC.sin_psi;
  y_C_p = v_C*AC.sin_psi + u_C*AC.cos_psi;
  x_P_p = x_C_p - BASE_LENGTH*psi_p*AC.sin_psi;
  y_P_p = y_C_p + BASE_LENGTH*psi_p*AC.cos_psi;

  // Calculate relative position and velocity
  AC.x_RP = x_P - x_R;
  AC.y_RP = y_P - y_R;
  AC.x_RP_p = x_P_p - x_R_p;
  AC.y_RP_p = y_P_p - y_R_p;

  AC.calculatePotentialForce();
  AC.calculateBrakingForce();
  AC.calculateInertialForce(x_R_pp, y_R_pp);
  AC.calculateInnerForce(u_C, v_C, psi_p);
  AC.calculateOuterForce(z_u, z_v, z_r);
  AC.calculateResultantForce();

  // Force -> controls conversion
  AC.dot_FR   = AC.F_x*BASE_LENGTH*AC.cos_psi + AC.F_y*BASE_LENGTH*AC.sin_psi;
  AC.cross_FR = AC.F_x*BASE_LENGTH*AC.sin_psi - AC.F_y*BASE_LENGTH*AC.cos_psi;

  AC.F_n = AC.dot_FR/BASE_LENGTH;
  AC.F_t = AC.cross_FR/BASE_LENGTH;

  u_1 =  AC.F_n;
  u_2 = -AC.F_t*INERTIA/(MASS*BASE_LENGTH);
  u_3 =  AC.throttle;
}


void FloatController::AutomaticControl::calculateInnerForce(double u_C, double v_C, double psi_p)
{
  F_c = MASS*BASE_LENGTH*pow(psi_p, 2);
  F_V_x = F_c*cos_psi - MASS*(v_C*psi_p*cos_psi + u_C*psi_p*sin_psi);
  F_V_y = F_c*sin_psi - MASS*(v_C*psi_p*sin_psi - u_C*psi_p*cos_psi);
}


void FloatController::AutomaticControl::calculateOuterForce(double u_p_dist, double v_p_dist, double psi_pp_dist)
{
  F_O_x = 0;
  F_O_y = 0;
}


void FloatController::AutomaticControl::calculateBrakingForce()
{
  norm_RP = sqrt(pow(x_RP, 2) + pow(y_RP, 2));
  if (norm_RP > epsilon)
  {
    dot_VR = x_RP_p*x_RP + y_RP_p*y_RP;
    cross_VR = x_RP_p*y_RP - y_RP_p*x_RP;

    // Normal and tangential velocity
    n_RP_p = dot_VR/norm_RP;
    t_RP_p = cross_VR/norm_RP;

    // Normal and tangential kinetic energies
    E_K_n = 0.5*MASS*pow(n_RP_p, 2);
    E_K_t = 0.5*MASS*pow(t_RP_p, 2);

    // Normal and tangential braking force
    F_B_n = -signum(n_RP_p)*k_B_n*E_K_n/norm_RP;
    F_B_t = -signum(t_RP_p)*k_B_t*E_K_t/norm_RP;
  }
  else
  {
    F_B_n = 0.0;
    F_B_t = 0.0;
  }

  phi_RP = atan2(y_RP, x_RP);
  F_B_x = F_B_n*cos(phi_RP) + F_B_t*sin(phi_RP);
  F_B_y = F_B_n*sin(phi_RP) - F_B_t*cos(phi_RP);
}


void FloatController::scaleControlSignals()
{
  double F_r, F_l;   // Left and right thrust force [N]

  F_r = (u_1 + u_2/FORCE_ARM)/2;
  F_l = (u_1 - u_2/FORCE_ARM)/2;

  if (absolute(F_r) < MIN_ENGINE_THRUST) F_r = 0.0;
  if (absolute(F_l) < MIN_ENGINE_THRUST) F_l = 0.0;

  u_1 = F_r + F_l;
  u_2 = FORCE_ARM*(F_r - F_l);

//  double s;

//  if (F_r < 0.0) F_l -= F_r;
//  if (F_l < 0.0) F_r -= F_l;

//  if (F_r > MAX_ENGINE_THRUST)
//  {
//    F_r = MAX_ENGINE_THRUST;
//    F_l -= (F_r - MAX_ENGINE_THRUST);
//  }
//  if (F_l > MAX_ENGINE_THRUST)
//  {
//    F_l = MAX_ENGINE_THRUST;
//    F_r -= (F_l - MAX_ENGINE_THRUST);
//  }

//  // Eliminate negative forces (shift forces above 0)
//  (F_l >= F_r) ? (s = F_r) : (s = F_l);
//  if (s < 0.0) {
//    F_r -= s;
//    F_l -= s;
//  }

//  // Eliminate too big forces (normalize them to <0 ; MAX_ENGINE_THRUST>)
//  (F_l <= F_r) ? (s = F_r/MAX_ENGINE_THRUST) : (s = F_l/MAX_ENGINE_THRUST);
//  if (s > 1) {
//    F_r /= s;
//    F_l /= s;
//  }

  //  u_1 -= abs(u_2)/FORCE_ARM;  // Because u_2 produces u_1
  //  if (u_1 < 0.0) u_1 = 0.0;
}


void FloatController::joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
{
  j_roll     = joy_msg->axes[0];
  j_pitch    = joy_msg->axes[1];
  j_throttle = joy_msg->axes[2];
}


void FloatController::stateCallback(const float_hc::State::ConstPtr& state_msg)
{
  x_C = state_msg->x;
  y_C = state_msg->y;
  psi = state_msg->psi;

  u_C = state_msg->u;
  psi = state_msg->v;
  psi_p = state_msg->psi_p;

  z_u = state_msg->z_u;
  z_v = state_msg->z_v;
  z_r = state_msg->z_r;
}


void FloatController::referenceCallback(const float_hc::Reference::ConstPtr& reference_msg)
{
  x_R   = reference_msg->x_r;
  y_R   = reference_msg->y_r;
  psi_R = reference_msg->psi_r;

  x_R_p   = reference_msg->x_r_p;
  y_R_p   = reference_msg->y_r_p;
  psi_R_p = reference_msg->psi_r_p;

  x_R_pp    = reference_msg->x_r_pp;
  y_R_pp    = reference_msg->y_r_pp;
  psi_R_pp  = reference_msg->psi_r_pp;
}


bool FloatController::paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res)
{
  switch (req.option)
  {
    case 1:
      controller_started = (req.value != 0);
    break;

    case 2:
      controls_scaling = (req.value != 0);
    break;

    case 3:
      if (req.value == 1) { control_type = CONSOLE;  u_1 = u_2 = u_3 = 0.0; }
      else if (req.value == 2) control_type = MANUAL;
      else if (req.value == 3) control_type = AUTOMATIC;
    break;

    case 4:
      MC.roll2rate = req.value;
    break;

    case 5:
      MC.pitch2speed = req.value;
    break;

    case 6:
      MC.k_p_u = req.value;
    break;

    case 7:
      MC.k_d_u = req.value;
    break;

    case 8:
      MC.k_p_r = req.value;
    break;

    case 9:
      MC.k_i_r = req.value;
      psi_d = psi;
      MC.u_i_r = 0.0;
    break;

    case 10:
      MC.k_d_r = req.value;
    break;

    case 11:
      AC.k_P = req.value;
    break;

    case 12:
      AC.k_B_t = req.value;
    break;

    case 13:
      AC.k_B_n = req.value;
    break;

    case 14:
      if (req.value >= 0 && req.value < MAX_THROTTLE) AC.throttle = req.value;
    break;

    case 20:
      u_1 = u_2 = u_3 = 0.0;
    break;

    case 21:
      if (control_type == CONSOLE)
      {
        u_1 = req.value;
        if (u_1 > MAX_THRUST) u_1 = MAX_THRUST;
        else if (u_1 < 0.0) u_1 = 0.0;
      }
    break;

    case 22:
      if (control_type == CONSOLE)
      {
        u_2 = req.value;
        if (u_2 > MAX_TORQUE) u_2 = MAX_TORQUE;
        else if (u_2 < -MAX_TORQUE) u_2 = -MAX_TORQUE;
      }
    break;

    case 23:
      if (control_type == CONSOLE)
      {
        u_3 = req.value;
        if (u_3 > MAX_THROTTLE) u_3 = MAX_THROTTLE;
        else if (u_3 < 0.0) u_3 = 0.0;
      }
    break;
  }

  ROS_INFO("Change: [%d][%f]", req.option, req.value);

  return true;
}


void FloatController::publishControlsMessage()
{
  controls_msg.header.stamp = ros::Time::now();

  if (u_1 < 0.0) controls_msg.u_1 = 0.0;
  else if (u_1 > MAX_THRUST) controls_msg.u_1 = MAX_THRUST;
  else controls_msg.u_1 = u_1;

  if (u_2 < -MAX_TORQUE) controls_msg.u_2 = -MAX_TORQUE;
  else if (u_2 > MAX_TORQUE) controls_msg.u_2 = MAX_TORQUE;
  else controls_msg.u_2 = u_2;

  if (u_3 < 0.0) controls_msg.u_3 = 0.0;
  else if (u_3 > MAX_THROTTLE) controls_msg.u_3 = MAX_THROTTLE;
  else controls_msg.u_3 = u_3;

  controls_pub.publish(controls_msg);
}

void FloatController::publishDesiredMessage()
{
  desired_msg.header.stamp = ros::Time::now();

  desired_msg.u_d = u_d;
  desired_msg.psi_d = psi_d;
  desired_msg.psi_p_d = psi_p_d;

  desired_pub.publish(desired_msg);
}
