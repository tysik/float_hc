#include "float_hc/float_stm.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_stm_node");
  ros::NodeHandle n;

  FloatSTM *STM = new FloatSTM(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
    STM->performTransmission();
    STM->publishImu();

    ros::spinOnce();
    loop_rate.sleep();
  }

  delete STM;

  return 0;
}

