#include "float_hc/Kalman.h"

using arma::mat;
using arma::vec;

KalmanFilter::KalmanFilter(uint dim_in, uint dim_out, uint dim_state)
{
  l = dim_in;
  m = dim_out;
  n = dim_state;

  initializeMatrices();
}


void KalmanFilter::initializeMatrices()
{
  A = mat(n,n).zeros();
  B = mat(n,l).zeros();
  C = mat(m,n).zeros();

  Q = mat(n,n).eye();
  R = mat(m,m).eye();
  P = mat(n,n).eye();

  K = mat(n,m).eye();

  u = vec(l).zeros();
  q_pred = vec(n).zeros();
  q_est = vec(n).zeros();
  y = vec(m).zeros();
}


void KalmanFilter::updateState()
{
  // Identity matrix
  mat I = arma::eye<mat>(n,n);

  // Predict State
  q_pred = A*q_est + B*u;
  P = A*P*trans(A) + Q;

  // Correct state
  K = P*trans(C)*inv(C*P*trans(C) + R);
  q_est = q_pred + K*(y - C*q_pred);
  P = (I - K*C)*P;
}
