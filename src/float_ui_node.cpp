#include "float_hc/float_ui.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_ui_node");
  ros::NodeHandle n;

  FloatUI *FU = new FloatUI(n);

  FU->displayUI();

  delete FU;

  return 0;
}
