#include "float_hc/float_reference.h"

using namespace float_hc;

FloatReference::FloatReference(ros::NodeHandle n) : nh(n)
{
  reference_pub = nh.advertise<float_hc::Reference>("float/reference", 5);
  params_srv = nh.advertiseService("float_reference_params", &FloatReference::paramsCallback, this);

  reference_started = true;
  reference_type = static_cast<ReferenceType>(REFERENCE_TYPE);

  // Lissajous trajectory
  liss_traj.T = LIS_T;
  liss_traj.w = 2*M_PI/liss_traj.T;
  liss_traj.n_x = LIS_N_X;      liss_traj.n_y = LIS_N_Y;
  liss_traj.R_x = LIS_R_X;  liss_traj.R_y = LIS_R_Y;
  liss_traj.x_o = 0.0;  liss_traj.y_o = 0.0;

  // Linear trajectory
  lin_traj.v = LIN_V;
  lin_traj.phi = LIN_PHI;
  lin_traj.x_o = 0.0;   lin_traj.y_o = 0.0;

  start_time = ros::Time::now();

  ROS_INFO("Float reference: start");
}


FloatReference::~FloatReference()
{
  ROS_INFO("Float reference: exit");
}


bool FloatReference::paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res)
{
  switch (req.option)
  {
    // Toggle reference trajectory generation
    case 1:
      reference_started = (req.value != 0);
    break;

    // Switch between point, linear and lissajous trajectory
    case 2:
      if (req.value == 1) reference_type = POINT;
      else if (req.value == 2) reference_type = LINEAR;
      else if (req.value == 3) reference_type = LISSAJOUS;
    break;

    // Change velocity of linear trajectory
    case 3:
      (req.value) >= 0 ? (lin_traj.v = req.value) : (lin_traj.v = -req.value);
    break;

    // Change orientation of motion
    case 4:
      lin_traj.phi = req.value;
    break;

    // Change period of lissajous trajectory
    case 5:
      if (req.value > 0)
      {
        liss_traj.T = req.value;
        liss_traj.w = 2*M_PI/liss_traj.T;
      }
    break;

    // Change radius in X of lissajous trajectory
    case 6:
      if (req.value >= 0) liss_traj.R_x = req.value;
    break;

    // Change radius in Y of lissajous trajectory
    case 7:
      if (req.value >= 0) liss_traj.R_y = req.value;
    break;

    // Change frequency X multiplier
    case 8:
      if (req.value >= 0) liss_traj.n_x = req.value;
    break;

    // Change frequency Y multiplier
    case 9:
      if (req.value >= 0) liss_traj.n_y = req.value;
    break;
  }

  start_time = ros::Time::now();  // Restart time counting if any changes happened

  ROS_INFO("Change: [%d][%f]", req.option, req.value);

  return true;
}


void FloatReference::calculateReferenceTrajectory()
{
  double v_x, v_y;
  double x_r_ppp, y_r_ppp;
  double delta_psi_r;

  actual_time = ros::Time::now();
  ros::Duration elapsed_time = actual_time - start_time;
  t = elapsed_time.toSec();

  switch (reference_type)
  {
    case POINT:
      x_R = 0.0;
      y_R = 0.0;
      psi_R = 0.0;

      x_R_p = 0.0;
      y_R_p = 0.0;
      psi_R_p = 0.0;

      x_R_pp = 0.0;
      y_R_pp = 0.0;
      psi_R_pp = 0.0;
    break;

    case LINEAR:
      v_x = lin_traj.v*cos(lin_traj.phi);
      v_y = lin_traj.v*sin(lin_traj.phi);

      x_R = lin_traj.x_o + v_x*t;
      y_R = lin_traj.y_o + v_y*t;
      psi_R = lin_traj.phi;

      x_R_p = v_x;
      y_R_p = v_y;
      psi_R_p = 0.0;

      x_R_pp = 0.0;
      y_R_pp = 0.0;
      psi_R_pp = 0.0;
    break;

    case LISSAJOUS:
      x_R = liss_traj.x_o + liss_traj.R_x*cos(liss_traj.n_x*liss_traj.w*t);
      y_R = liss_traj.y_o + liss_traj.R_y*sin(liss_traj.n_y*liss_traj.w*t);

      x_R_p = -liss_traj.n_x*liss_traj.w*liss_traj.R_x*sin(liss_traj.n_x*liss_traj.w*t);
      y_R_p =  liss_traj.n_y*liss_traj.w*liss_traj.R_y*cos(liss_traj.n_y*liss_traj.w*t);

      x_R_pp = -pow(liss_traj.n_x*liss_traj.w, 2)*liss_traj.R_x*cos(liss_traj.n_x*liss_traj.w*t);
      y_R_pp = -pow(liss_traj.n_y*liss_traj.w, 2)*liss_traj.R_y*sin(liss_traj.n_y*liss_traj.w*t);

      x_r_ppp =  pow(liss_traj.n_x*liss_traj.w, 3)*liss_traj.R_x*sin(liss_traj.n_x*liss_traj.w*t);
      y_r_ppp = -pow(liss_traj.n_y*liss_traj.w, 3)*liss_traj.R_y*cos(liss_traj.n_x*liss_traj.w*t);

      // Continous psi_r
      delta_psi_r = atan2(y_R_p, x_R_p) - atan2(sin(psi_R), cos(psi_R));
      if (delta_psi_r > M_PI)
        delta_psi_r -= 2*M_PI;
      else if (delta_psi_r < -M_PI)
        delta_psi_r += 2*M_PI;
      psi_R += delta_psi_r;

      // TODO: Check equations because the do not work correctly
      if (pow(x_R_p, 2) + pow(y_R_p,2) != 0)
      {
        psi_R_p  = (y_R_pp*x_R_p - x_R_pp*y_R_p)/(pow(x_R_p, 2) + pow(y_R_p,2));
        psi_R_pp = ((y_r_ppp*x_R_p - x_r_ppp*y_R_p)*(pow(x_R_p, 2) + pow(y_R_p, 2)) - 2*(y_R_pp*x_R_p - x_R_pp*y_R_p)*(x_R_p + y_R_p))/(pow(pow(x_R_p, 2) + pow(y_R_p, 2), 2));
      }
    break;
  }
}


void FloatReference::publishReferenceMessage()
{
  reference_msg.header.stamp = ros::Time::now();

  reference_msg.x_r = x_R;
  reference_msg.y_r = y_R;
  reference_msg.psi_r = psi_R;

  reference_msg.x_r_p = x_R_p;
  reference_msg.y_r_p = y_R_p;
  reference_msg.psi_r_p = psi_R_p;

  reference_msg.x_r_pp = x_R_pp;
  reference_msg.y_r_pp = y_R_pp;
  reference_msg.psi_r_pp = psi_R_pp;

  reference_msg.t = t;

  reference_pub.publish(reference_msg);
}


void FloatReference::publishReferenceTransform()
{
  reference_tf.setOrigin(tf::Vector3(x_R, y_R, 0.0));
  reference_tf.setRotation(tf::createQuaternionFromYaw(psi_R));
  tf_br.sendTransform(tf::StampedTransform(reference_tf, ros::Time::now(), "world", "reference"));
}
