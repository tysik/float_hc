#include "float_hc/float_logs.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_logs_node");
  ros::NodeHandle n;

  FloatLogs *FL = new FloatLogs(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
    if (FL->registering_data)
      FL->saveDataToFile();

    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
