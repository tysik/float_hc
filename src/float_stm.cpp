#include "float_hc/float_stm.h"

using namespace float_hc;

FloatSTM::FloatSTM(ros::NodeHandle n) : nh(n)
{
  imu_pub = nh.advertise<sensor_msgs::Imu>("float/imu", 5);
  controls_sub = nh.subscribe<float_hc::Controls>("float/controls", 5, &FloatSTM::controlsCallback, this);
  joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 5, &FloatSTM::joyCallback, this);
  params_srv = nh.advertiseService("float_stm_params", &FloatSTM::paramsCallback, this);

  UART_port = 16;     // Device on /dev/ttyUSB0

  prepareTXBuffer(0.0, 0.0, 0.0);

  memset(RX_buffer, 0, RX_BUFFER_SIZE);
  memset(&stm_data, 0, sizeof(stm_data));

  openUART();
  ros::Duration(0.1).sleep();

  ROS_INFO("Float STM: start");
}


FloatSTM::~FloatSTM()
{
  closeUART();
  ROS_INFO("Float STM: exit");
}


void FloatSTM::controlsCallback(const float_hc::Controls::ConstPtr& controls_msg)
{
  double u_1, u_2, u_3;

  u_1 = controls_msg->u_1;    // Pushing
  u_2 = controls_msg->u_2;    // Orienting
  u_3 = controls_msg->u_3;    // Hover

  prepareTXBuffer(u_1, u_2, u_3);
}


void FloatSTM::joyCallback(const sensor_msgs::Joy::ConstPtr& joy_msg)
{
  // Set STM
  if (joy_msg->buttons[2]) stm_data.mode |= 0xF0;  // Button number 3 on V.1 Joystick
  else stm_data.mode &= 0x0F;

  // Reset STM
  if (joy_msg->buttons[3]) stm_data.mode |= 0x0F;  // Button number 4 on V.1 Joystick
  else stm_data.mode &= 0xF0;
}


bool FloatSTM::paramsCallback(float_hc::Params::Request &req, float_hc::Params::Response &res)
{
  if (req.option == 1) // Set STM
  {
    if (req.value != 0) stm_data.mode |= 0xF0;
    else stm_data.mode &= 0x0F;
  }
  else if (req.option == 2) // Reset STM
  {
    if (req.value != 0) stm_data.mode |= 0x0F;
    else stm_data.mode &= 0xF0;
  }

  ROS_INFO("Change: [%d][%f]", req.option, req.value);

  return true;
}


void FloatSTM::performTransmission()
{
  sendData();
  readData();
  convertRawData();
}


void FloatSTM::convertRawData()
{
  USHORT crc = 0;             // Redundancy check (2 bytes)
  USHORT frame_crc = 0;
  UCHAR aux_data[] = {0, 0};  // Auxiliary data for CRC algorithm

  // Check the header
  if (RX_buffer[0] == 0xAA && RX_buffer[1] == 0xBB)
  {
    // Calculate CRC
    for (int i=2; i < RX_BUFFER_SIZE-2; i++)
    {
      aux_data[1] = aux_data[0];
      aux_data[0] = RX_buffer[i];

      if (crc & 0x8000)
      {
        crc = (crc & 0x7fff)*2;
        crc ^= 0x8005;
      }
      else crc *= 2;

      crc ^= aux_data[0] + aux_data[1]*256;
    }

    // CRC sent by STM
    frame_crc = RX_buffer[29] + RX_buffer[30]*256;

    // Convert raw data into values
    if (crc == frame_crc)
    {
      stm_data.linear_acc[0] = static_cast<double>(RX_buffer[2] + RX_buffer[3]*256 + RX_buffer[4]*65536)/1000 - 2;
      stm_data.linear_acc[1] = static_cast<double>(RX_buffer[5] + RX_buffer[6]*256 + RX_buffer[7]*65536)/1000 - 2;
      stm_data.linear_acc[2] = static_cast<double>(RX_buffer[8] + RX_buffer[9]*256 + RX_buffer[10]*65536)/1000 - 2;

      stm_data.angular_rate[0] = static_cast<double>(RX_buffer[11] + RX_buffer[12]*256 + RX_buffer[13]*65536)/1000 - 500;
      stm_data.angular_rate[1] = static_cast<double>(RX_buffer[14] + RX_buffer[15]*256 + RX_buffer[16]*65536)/1000 - 500;
      stm_data.angular_rate[2] = static_cast<double>(RX_buffer[17] + RX_buffer[18]*256 + RX_buffer[19]*65536)/1000 - 500;

      stm_data.roll  = static_cast<double>(RX_buffer[20] + RX_buffer[21]*256 + RX_buffer[22]*65536)/1000 - 180;
      stm_data.pitch = static_cast<double>(RX_buffer[23] + RX_buffer[24]*256 + RX_buffer[25]*65536)/1000 - 180;
      stm_data.yaw   = static_cast<double>(RX_buffer[26] + RX_buffer[27]*256 + RX_buffer[28]*65536)/1000 - 180;
    }
    else
      ROS_WARN("CRC test of STM data failed.");
  }
  else
    ROS_WARN("Header of STM data is not correct.");
}


void FloatSTM::prepareTXBuffer(double u_1, double u_2, double u_3)
{
  UCHAR aux_data[] = {0, 0};
  USHORT crc = 0;

  // Convert the values into integers
  TX_buffer[0] = 0xAA;
  TX_buffer[1] = 0xBB;
  TX_buffer[2] = (static_cast<int>((u_1+10)*1000)) & 0x00FF;
  TX_buffer[3] = ((static_cast<int>((u_1+10)*1000)) & 0xFF00) >> 8;
  TX_buffer[4] = (static_cast<int>((u_2+10)*1000)) & 0x00FF;
  TX_buffer[5] = ((static_cast<int>((u_2+10)*1000)) & 0xFF00) >> 8;
  TX_buffer[6] = (static_cast<int>((u_3)*100)) & 0x00FF;
  TX_buffer[7] = ((static_cast<int>((u_3)*100)) & 0xFF00) >> 8;
  TX_buffer[8] = stm_data.mode;

  // Calculate CRC
  for (int i=2 ; i < TX_BUFFER_SIZE-2 ; i++)
  {
    aux_data[1] = aux_data[0];
    aux_data[0] = TX_buffer[i];

    if (crc & 0x8000)
    {
      crc = (crc & 0x7fff)*2;
      crc ^= 0x8005;
    }
    else crc *= 2;

    crc ^= aux_data[0] + aux_data[1]*256;
  }

  // Write CRC bytes into buffer
  TX_buffer[9] = crc & 0x00FF;
  TX_buffer[10] = ((crc & 0xFF00) >> 8);

  stm_data.mode = 0x00;
}


void FloatSTM::openUART()
{
  if (!RS232_OpenComport(UART_port, 115200))
    ROS_INFO("COM port opened.");
  else
  {
    ROS_ERROR(" [-] COM port %d couldn't be opened.", UART_port);
    return; //ros::shutdown();
  }
}


void FloatSTM::closeUART()
{
  RS232_CloseComport(UART_port);
}


void FloatSTM::sendData()
{
  RS232_SendBuf(UART_port, TX_buffer, TX_BUFFER_SIZE);
}


void FloatSTM::readData()
{
  RS232_PollComport(UART_port, RX_buffer, RX_BUFFER_SIZE);
}


void FloatSTM::publishImu()
{
  imu_msg.header.stamp = ros::Time::now();
  imu_msg.angular_velocity.z = stm_data.angular_rate[2]*M_PI/180.0;
  imu_pub.publish(imu_msg);
}
