#include "float_hc/float_controller.h"

using namespace float_hc;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "float_controller_node");
  ros::NodeHandle n;

  FloatController *FC = new FloatController(n);

  ros::Rate loop_rate(SYS_FREQ);

  while (ros::ok())
  {
    if (FC->controller_started)
    {
      FC->computeControls();
      FC->publishControlsMessage();
      FC->publishDesiredMessage();
    }

    ros::spinOnce();
    loop_rate.sleep();
  }

  delete FC;

  return 0;
}
