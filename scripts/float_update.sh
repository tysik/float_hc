LocalFolder=/home/tysik/catkin_ws/src/float_hc/
RemoteFolder=/home/float/catkin_ws/src/float_hc/

# Remove old version
ssh float@Float_PC "rm -r $RemoteFolder"
ssh float@Float_PC "mkdir $RemoteFolder && mkdir ${RemoteFolder}include/"
ssh float@Float_PC "rm -r /home/float/catkin_ws/build/float_hc/"
ssh float@Float_PC "rm -r /home/float/catkin_ws/devel/include/float_hc/"
ssh float@Float_PC "rm -r /home/float/catkin_ws/devel/share/float_hc/"
ssh float@Float_PC "rm -r /home/float/catkin_ws/devel/lib/float_hc/"

# Temporarily move scan_tools
ssh float@Float_PC "mv /home/float/catkin_ws/src/scan_tools/ /home/float/"

## Copy new version
scp -r ${LocalFolder}src/ float@Float_PC:${RemoteFolder}src/
scp -r ${LocalFolder}srv/ float@Float_PC:${RemoteFolder}srv/
scp -r ${LocalFolder}msg/ float@Float_PC:${RemoteFolder}msg/
scp -r ${LocalFolder}launch/ float@Float_PC:${RemoteFolder}launch/
scp -r ${LocalFolder}scripts/ float@Float_PC:${RemoteFolder}scripts/
scp -r ${LocalFolder}resources/ float@Float_PC:${RemoteFolder}resources/
scp -r ${LocalFolder}include/float_hc/ float@Float_PC:${RemoteFolder}include/float_hc/

scp ${LocalFolder}CMakeLists.txt float@Float_PC:${RemoteFolder}CMakeLists.txt
scp ${LocalFolder}package.xml float@Float_PC:${RemoteFolder}package.xml

# Build new version
ssh float@Float_PC "source /opt/ros/hydro/setup.bash && cd /home/float/catkin_ws/ && catkin_make --pkg float_hc"

# Move back scan_tools
ssh float@Float_pc "mv /home/float/scan_tools/ /home/float/catkin_ws/src/"
