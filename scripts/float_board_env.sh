#!/bin/sh

export ROS_IP="192.168.0.120"
export ROS_HOSTNAME="Float_PC"
export ROS_MASTER_URI="http://192.168.0.120:11311"
export ROSLAUNCH_SSH_UNKNOWN="1"

. /home/$USER/catkin_ws/devel/setup.sh
exec "$@"
