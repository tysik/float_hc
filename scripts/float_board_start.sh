# This script sets the ROS environmental variables for uPC computer and starts roscore there.
# The file should be in folder /home/float/catkin_ws/src/float_hc/scripts/

export ROS_HOSTNAME="Float_PC";
export ROS_IP="192.168.0.120";
export ROS_MASTER_URI="http://192.168.0.120:11311";
export ROSLAUNCH_SSH_UNKNOWN="1";

source /opt/ros/hydro/setup.bash

roscore
