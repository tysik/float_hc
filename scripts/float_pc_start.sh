# This script sets the ROS environmental variables for local computer, sshs into FloatPC and starts the onboard script there. Then it launches the Float with a certain roslaunch file.
# It is adviced to create an alias for the script execution, e.g. alias float_start="source /home/user/catkin_ws/src/float_hc/float_pc_start.sh".
# The file should be in folder /home/use/catkin_ws/src/float_hc/scripts/.

echo "Starting FLOAT";

export ROS_HOSTNAME="Tysik_PC";
export ROS_IP="192.168.0.140";
export ROS_MASTER_URI="http://192.168.0.120:11311";
export ROSLAUNCH_SSH_UNKNOWN="1";

echo "ROS_MASTER_URI: " $ROS_MASTER_URI
echo "ROS_IP: " $ROS_IP

echo "Starting roscore on FloatPC"
ssh float@Float_PC "source /home/float/catkin_ws/src/float_hc/scripts/float_board_start.sh" &
sleep 10;
echo "Roscore OK";

roslaunch float_hc float_hc.launch
