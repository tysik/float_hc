Float Hovercraft

The Float robot poses a SBC (Single Board Computer) on its board. The computer uses the ROS (Robot Operating System) installed over Ubuntu 12.04 (x32). The version of ROS is Hydro. 

***
HOW TO START FLOAT

1. Power on the dedicated Float router.

2. Plug in the battery and wait about a minute untill the SBC has started. The WiFi on the Float should light up the green diode and connect automaticaly to the "float_router" network.

3. On the local computer connect to the "float_router" (the IP must be set to 192.168.0.140, the password is floatxxx). 

4. Type "float_start" in the command line (an alias for sourcing float_pc_start.sh). You should see the User Interface in your terminal. Press the blue button on the STM board to start the low level controller.

The default control system is Manual so you can now control the hovercraft with joystick.

***

1. Nodes
	1.1. float_stm_node - this node is responsible for communication between onboard PC and the STM electronic board via RS232 standard. It subscribes data of type float_hc::Controls. It collects the IMU data and converts it into sensors_msgs::Imu message (then publish).

	1.2. float_controller_node - this node executes the control algorithms, both for manual control as well as automatic one. It is also responsible for data logging. It subscribes data of type: sensor_msgs::Joy, float_hc::State, float_hc::Reference and. It publishes data of type float_hc::Controls.
	
	1.3 float_state_estimator_node - this node collects the measurements (geometry_msgs::Pose2D and sensor_msgs::Imu) as well as control signals (float_hc::Controls) and converts them into estimate of FLoat state with the use of Kalman filter
 
	1.4. float_reference_node - this node generates a reference trajectory and publishes data of type: float_hc::Reference. There are three possible types of reference trajectory, i.e. a point-like trajectory, linear motion and a Lissajous curve.

	1.5. float_ui_node - this node is a control panel for the Float. It allows the user to switch between console, manual and automatic control modes, start logging the data, change the gains of controllers and the settings of reference trajectories. It doesn't use any publish-subscribe data exchange. Instead, it uses the service-client way of communication between nodes.
	
	1.5 - float_logs_node - the node to collect all the data and store them in a .txt file.


2. Environmental settings
	3.1. Hokuyo - after pluging in the laser USB cable it is seen in ubuntu as a new device located at /dev/ttyACM0. In order to ensure that the hokuyo_node will be able to use it, it needs to poses proper privileges (rights). It can be done via "sudo chmod a+rw /dev/ttyACM0" each time the cable is plugged in, or simply via adding the user to a dialout group "sudo adduser $USER dialout".

	3.2. WiFi connection - the user can connect to the onboard PC via ssh. The connection on SBC is set to automaticaly connect with "float_router" network. The onboar PC has IP: 192.168.150.120. The $ROS_IP and $ROS_MASTER_URI need to be set properly.